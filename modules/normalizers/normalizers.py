import abc

import tensorflow as tf

from ailib.modules import Module
from ailib.utilities import spaces

__all__ = ['Normalizer', 'RunningMeanNormalizer']


class Normalizer(Module, metaclass=abc.ABCMeta):
    def __init__(self, name=None):
        super().__init__(name)

    @abc.abstractmethod
    def normalize(self, value):
        pass

    @abc.abstractmethod
    def denormalize(self, value):
        pass

    @abc.abstractmethod
    def get_update_op(self, value):
        pass

    @abc.abstractmethod
    def build(self):
        pass


class RunningMeanNormalizer(Normalizer):
    def __init__(self, space: spaces.Space, epsilon=1e-6, name=None):
        super().__init__(name)
        self._space = space
        self._epsilon = epsilon
        self._sum0, self._sum1, self._sum2 = None, None, None
        self._mean, self._std = None, None

    @Module.in_scope_wrapper
    def build(self):
        if isinstance(self._space, spaces.BasicSpace):
            self._sum0 = tf.get_variable('sum0', self._space.shape, tf.float32,
                                         tf.constant_initializer(self._epsilon), trainable=False,
                                         collections=[tf.GraphKeys.GLOBAL_VARIABLES,
                                                      tf.GraphKeys.MOVING_AVERAGE_VARIABLES])
            self._sum1 = tf.get_variable('sum1', self._space.shape, tf.float32,
                                         tf.constant_initializer(), trainable=False,
                                         collections=[tf.GraphKeys.GLOBAL_VARIABLES,
                                                      tf.GraphKeys.MOVING_AVERAGE_VARIABLES])
            self._sum2 = tf.get_variable('sum2', self._space.shape, tf.float32,
                                         tf.constant_initializer(self._epsilon), trainable=False,
                                         collections=[tf.GraphKeys.GLOBAL_VARIABLES,
                                                      tf.GraphKeys.MOVING_AVERAGE_VARIABLES])
            self._mean = self._sum1 / self._sum0
            self._std = tf.sqrt(tf.maximum(self._sum2 / self._sum0 - tf.square(self._mean), self._epsilon))
        else:
            assert isinstance(self._space, spaces.HybridSpace)
            self._sum0 = [tf.get_variable(f'sum0_{index}', space.shape, tf.float32,
                                          tf.constant_initializer(self._epsilon), trainable=False,
                                          collections=[tf.GraphKeys.GLOBAL_VARIABLES,
                                                       tf.GraphKeys.MOVING_AVERAGE_VARIABLES])
                          if isinstance(space, spaces.Box) else None
                          for index, space in enumerate(self._space.spaces)]
            self._sum1 = [tf.get_variable(f'sum1_{index}', space.shape, tf.float32,
                                          tf.constant_initializer(self._epsilon), trainable=False,
                                          collections=[tf.GraphKeys.GLOBAL_VARIABLES,
                                                       tf.GraphKeys.MOVING_AVERAGE_VARIABLES])
                          if isinstance(space, spaces.Box) else None
                          for index, space in enumerate(self._space.spaces)]
            self._sum2 = [tf.get_variable(f'sum2_{index}', space.shape, tf.float32,
                                          tf.constant_initializer(self._epsilon), trainable=False,
                                          collections=[tf.GraphKeys.GLOBAL_VARIABLES,
                                                       tf.GraphKeys.MOVING_AVERAGE_VARIABLES])
                          if isinstance(space, spaces.Box) else None
                          for index, space in enumerate(self._space.spaces)]
            self._mean = [sum1 / sum0
                          if isinstance(space, spaces.Box) else None
                          for sum0, sum1, space in zip(self._sum0, self._sum1, self._space.spaces)]
            self._std = [tf.sqrt(tf.maximum(sum2 / sum0 - tf.square(mean), self._epsilon))
                         if isinstance(space, spaces.Box) else None
                         for sum0, sum2, mean, space in zip(self._sum0, self._sum2, self._mean, self._space.spaces)]

    @Module.assert_scope_built
    def get_update_op(self, value):
        if isinstance(self._space, spaces.BasicSpace):
            return [
                tf.assign_add(self._sum0, tf.reduce_sum(tf.ones_like(value), axis=0)),
                tf.assign_add(self._sum1, tf.reduce_sum(value, axis=0)),
                tf.assign_add(self._sum2, tf.reduce_sum(tf.square(value), axis=0))
            ]
        else:
            assert isinstance(self._space, spaces.HybridSpace)
            results = [tf.assign_add(sum0, tf.reduce_sum(tf.ones_like(v), axis=0))
                       if isinstance(space, spaces.Box) else None
                       for sum0, space, v in zip(self._sum0, self._space.spaces, value)]
            results += [tf.assign_add(sum1, tf.reduce_sum(v, axis=0))
                        if isinstance(space, spaces.Box) else None
                        for sum1, space, v in zip(self._sum1, self._space.spaces, value)]
            results += [tf.assign_add(sum2, tf.reduce_sum(tf.square(v), axis=0))
                        if isinstance(space, spaces.Box) else None
                        for sum2, space, v in zip(self._sum2, self._space.spaces, value)]
            return results

    @property
    @Module.assert_scope_built
    def sums(self):
        return tuple(zip(self._sum0, self._sum1, self._sum2))

    @Module.assert_scope_built
    def sums_value(self, session: tf.Session):
        return session.run(self.sums)

    @property
    @Module.assert_scope_built
    def mean(self):
        return self._mean

    @property
    @Module.assert_scope_built
    def std(self):
        return self._std

    @Module.assert_scope_built
    def normalize(self, value):
        if isinstance(self._space, spaces.BasicSpace):
            return (value - self._mean) / self._std
        else:
            assert isinstance(self._space, spaces.HybridSpace)
            return [(v - mean) / std
                    if isinstance(space, spaces.Box) else v
                    for v, mean, std, space in zip(value, self._mean, self._std, self._space.spaces)]

    @Module.assert_scope_built
    def denormalize(self, value):
        if isinstance(self._space, spaces.BasicSpace):
            return value * self._std + self._mean
        else:
            assert isinstance(self._space, spaces.HybridSpace)
            return [value * std + mean
                    if isinstance(space, spaces.Box) else v
                    for v, mean, std, space in zip(value, self._mean, self._std, self._space.spaces)]

    @Module.assert_scope_built
    def mean_value(self, session: tf.Session):
        return session.run(self._mean)

    @Module.assert_scope_built
    def std_value(self, session: tf.Session):
        return session.run(self._std)
