import abc
import copy
import inspect

import tensorflow as tf

__all__ = ['Module']


class Module(object, metaclass=abc.ABCMeta):
    __module_manager = dict()

    @staticmethod
    def assert_scope_built(fn):
        def _call(self: 'Module', *args, **kwargs):
            assert self._scope_built, f"You must build the `{self.__class__.__name__}' before call `{fn.__name__}'"
            return fn(self, *args, **kwargs)

        return _call

    @staticmethod
    def assert_scope_not_built(fn):
        def _call(self: 'Module', *args, **kwargs):
            assert not self._scope_built, f"Calling `{fn.__name__}' after `{self.__class__.__name__}' built is " \
                                          f"prohibited "
            return fn(self, *args, **kwargs)

        return _call

    @staticmethod
    def in_scope_wrapper(fn):
        if 'reuse' in inspect.signature(fn).parameters:
            def _call(self: 'Module', *args, reuse=False, **kwargs):
                if self._scope is None:
                    self._scope = tf.variable_scope(self._name)
                with self._scope as new_scope:
                    scope_path = tf.get_variable_scope().name
                    if not self._scope_path:
                        self._scope_path = scope_path
                    if reuse:
                        new_scope.reuse_variables()
                    result = fn(self, *args, reuse=reuse, **kwargs)
                    self._scope_built = True
                    return result

            return _call
        else:
            def _call(self: 'Module', *args, reuse=False, **kwargs):
                if self._scope is None:
                    self._scope = tf.variable_scope(self._name)
                with self._scope as new_scope:
                    scope_path = tf.get_variable_scope().name
                    if not self._scope_path:
                        self._scope_path = scope_path
                    if reuse:
                        new_scope.reuse_variables()
                    result = fn(self, *args, **kwargs)
                    self._scope_built = True
                    return result

            return _call

    def __init__(self, name=None):
        self._name = name if name else self.__class__.__name__
        if self._name in self.__module_manager:
            self.__module_manager[self._name] += 1
            self._name += str(self.__module_manager[self._name])
        else:
            self.__module_manager.setdefault(self._name, 0)

        self._scope = None
        self._scope_path = None
        self._scope_built = False

    @property
    def scope_built(self):
        return self._scope_built

    @property
    def scope_path(self):
        return self._scope_path

    @property
    @assert_scope_built.__get__(object, 'Module')
    def global_variables(self):
        return tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self._scope_path)

    @property
    @assert_scope_built.__get__(object, 'Module')
    def regularize_variables(self):
        return []

    @property
    @assert_scope_built.__get__(object, 'Module')
    def trainable_variables(self):
        return tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self._scope_path)

    @property
    @assert_scope_built.__get__(object, 'Module')
    def local_variables(self):
        return tf.get_collection(tf.GraphKeys.LOCAL_VARIABLES, scope=self._scope_path)

    @property
    @assert_scope_built.__get__(object, 'Module')
    def moving_average_variables(self):
        return tf.get_collection(tf.GraphKeys.MOVING_AVERAGE_VARIABLES, scope=self._scope_path)

    def __copy__(self):
        newone = type(self).__new__(self.__class__)
        for k, v in self.__dict__.items():
            if isinstance(v, Module):
                setattr(newone, k, copy.copy(v))
            else:
                setattr(newone, k, v)
        newone._scope = None
        newone._scope_built = False
        newone._scope_path = None
        return newone
