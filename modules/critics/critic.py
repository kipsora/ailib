import abc
import copy

from ailib.modules import Module
from ailib.utilities.environments import base

__all__ = ['Critic']


class Critic(Module, metaclass=abc.ABCMeta):
    def __init__(self, env: base.Environment, name=None):
        super().__init__(name)
        self._env = env

    @property
    @Module.assert_scope_built
    def kernel_variables(self):
        return tuple(filter(lambda v: 'kernel' in v.name, self.trainable_variables))

    @property
    def popart_variables(self):
        raise SyntaxError("This critic has not implemented popart_variables.")

    def target_copy(self, prefix='Target'):
        result = copy.copy(self)
        result._name = prefix + '/' + self._name
        return result
