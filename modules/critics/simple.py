import copy

import tensorflow as tf

from ailib.modules import critics, nn, Module
from ailib.utilities import spaces

__all__ = ['SimpleCritic']


class SimpleCritic(critics.Critic):
    def __init__(self, env,
                 obs_network: nn.MLPModule=None,
                 act_network: nn.MLPModule=None,
                 common_network: nn.MLPModule=None,
                 name=None):
        super().__init__(env, name)

        self._act_space = self._env.action_space
        self._obs_space = self._env.observation_space
        assert isinstance(self._act_space, spaces.Box)

        if isinstance(self._obs_space, spaces.BasicSpace):
            self._obs_network = obs_network if obs_network else nn.MLPModule(units=(1024, 512))
        else:
            assert isinstance(self._obs_space, spaces.HybridSpace)
            self._obs_network = obs_network if obs_network else [nn.MLPModule(units=(1024, 512))
                                                                 for _ in self._obs_space.spaces]
        self._act_network = act_network if act_network else nn.MLPModule(units=(256, 256))
        self._common_network = common_network if common_network else nn.MLPModule(units=(256,))

        self._popart_variables = None

    def target_copy(self, prefix='Target'):
        result = super(SimpleCritic, self).target_copy(prefix)
        if isinstance(self._obs_space, spaces.HybridSpace):
            result._obs_network = [copy.copy(network) for network in self._obs_network]
        return result

    @property
    @Module.assert_scope_built
    def popart_variables(self):
        return self._popart_variables

    @Module.in_scope_wrapper
    def apply(self, obs, act):
        if isinstance(self._obs_space, spaces.BasicSpace):
            x = self._obs_network(obs)
        else:
            x = tf.concat([network(o) for network, o in zip(self._obs_network, obs)], axis=1)
        y = self._act_network(act)
        h = tf.concat((x, y), axis=1)
        h = self._common_network(h)

        layer = tf.layers.Dense(units=1)
        h = layer(h)
        h = tf.reshape(h, shape=[-1])
        self._popart_variables = [layer.kernel, layer.bias]
        return h
