import collections

import tensorflow as tf

from ailib.modules import nn, Module
from ailib.utilities import helpers

__all__ = ['MLPModule']


class MLPModule(nn.NNModule):
    def __init__(self, units=(64, 64, 64), activation='relu', layer_norm=False, last_no_activation=False, name=None):
        super().__init__(name)

        self._units = tuple(units)
        self._layers = len(self._units) - int(last_no_activation)

        if callable(activation):
            self._activation = [activation() for _ in range(self._layers)]
        elif isinstance(activation, str):
            self._activation = [helpers.get_activation_from_str(activation) for _ in range(self._layers)]
        elif isinstance(activation, collections.Iterable):
            self._activation = [helpers.get_activation_from_str(a) for a in self._activation]
            assert len(self._activation) == self._layers
        else:
            raise NotImplementedError

        if self._units and last_no_activation:
            self._activation.append(None)

        self._layer_norm = layer_norm

    @Module.in_scope_wrapper
    def __call__(self, x):
        import tensorflow.contrib as tc
        if len(x.shape) == 1:
            x = tf.expand_dims(x, axis=1)
        else:
            x = tf.layers.flatten(x)
        for i, a in zip(self._units, self._activation):
            x = tf.layers.dense(x, units=i)
            if self._layer_norm:
                x = tc.layers.layer_norm(x)
            x = a(x) if a else x
        return x
