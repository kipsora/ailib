import abc

from ailib import modules

__all__ = ['NNModule']


class NNModule(modules.Module):
    def __init__(self, name=None):
        super().__init__(name)

    @abc.abstractmethod
    def __call__(self, *args, **kwargs):
        pass
