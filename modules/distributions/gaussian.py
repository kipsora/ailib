import numpy as np
import tensorflow as tf

from ailib.modules import distributions, Module
from ailib.utilities import helpers, spaces

__all__ = ['DiagonalGaussian']


class DiagonalGaussian(distributions.Distribution):
    def __init__(self, space: spaces.Space, name=None):
        super().__init__(name)

        self._space = space
        self._mean, self._std, self._logstd = None, None, None
        self._sample, self._entropy = None, None

    @Module.in_scope_wrapper
    @Module.assert_scope_not_built
    def build(self, latent):
        if isinstance(self._space, spaces.BasicSpace):
            shape = self._space.shape
            act_num = np.prod(shape)
            self._mean = tf.layers.dense(latent, units=act_num, activation=tf.nn.tanh)
            self._logstd = tf.get_variable('logstd', shape=[act_num], dtype=tf.float32,
                                           trainable=True, initializer=tf.zeros_initializer(),
                                           collections=[tf.GraphKeys.GLOBAL_VARIABLES,
                                                        tf.GraphKeys.TRAINABLE_VARIABLES])
            self._std = tf.exp(self._logstd)

            self._sample = tf.reshape(self._mean + self._std * tf.random_normal(tf.shape(self._mean)),
                                      shape=(-1,) + shape)
            self._entropy = tf.reduce_sum(self._logstd + 0.5 * np.log(2.0 * np.pi * np.e), axis=-1)
        else:
            assert isinstance(self._space, spaces.HybridSpace)
            self._mean, self._std, self._logstd, self._sample, self._entropy = [], [], [], [], []
            for shape in self._space.shapes:
                act_num = np.prod(shape)
                mean = tf.layers.dense(latent, units=act_num, activation=tf.nn.tanh)
                logstd = tf.get_variable('logstd', shape=[act_num], dtype=tf.float32,
                                         trainable=True, initializer=tf.zeros_initializer(),
                                         collections=[tf.GraphKeys.GLOBAL_VARIABLES, tf.GraphKeys.TRAINABLE_VARIABLES])
                std = tf.exp(self._logstd)
                sample = tf.reshape(self._mean + self._std * tf.random_normal(tf.shape(self._mean)),
                                    shape=(-1,) + shape)
                entropy = tf.reduce_sum(self._logstd + 0.5 * np.log(2.0 * np.pi * np.e), axis=-1)
                self._mean.append(mean)
                self._logstd.append(logstd)
                self._std.append(std)
                self._sample.append(sample)
                self._entropy.append(entropy)

    @property
    @Module.assert_scope_built
    def sample(self):
        return self._sample

    @property
    @Module.assert_scope_built
    def logstd(self):
        return self._logstd

    @property
    @Module.assert_scope_built
    def std(self):
        return self._std

    @property
    @Module.assert_scope_built
    def mean(self):
        return self._mean

    @Module.assert_scope_built
    def kl_distance(self, other):
        assert isinstance(other, self.__class__)
        if isinstance(self._space, spaces.BasicSpace):
            assert isinstance(other._space, spaces.BasicSpace)
            return tf.reduce_sum(other.logstd - self.logstd + (tf.square(self.std) + tf.square(
                self.mean - other.mean)) / (2.0 * tf.square(other.std)) - 0.5, axis=-1)
        else:
            assert isinstance(self._space, spaces.HybridSpace) and isinstance(other._space, spaces.HybridSpace)
            return tuple(
                tf.reduce_sum(
                    ologstd - logstd + (tf.square(std) + tf.square(mean - omean)) / (2.0 * tf.square(ostd)) - 0.5,
                    axis=-1
                )
                for logstd, std, mean, ostd, ologstd, omean in
                zip(self._logstd, self._std, self._mean, other.std, other.logstd, other.mean)
            )

    @Module.assert_scope_built
    def neg_log(self, other):
        if isinstance(self._space, spaces.BasicSpace):
            return 0.5 * tf.reduce_sum(tf.square((other - self._mean) / self._std), axis=-1) \
                   + 0.5 * np.log(2.0 * np.pi) * tf.to_float(tf.shape(other)[-1]) \
                   + tf.reduce_sum(self._logstd, axis=-1)
        else:
            return tuple(
                0.5 * tf.reduce_sum(tf.square((other - mean) / std), axis=-1) + 0.5 * np.log(
                    2.0 * np.pi) * tf.to_float(tf.shape(other)[-1]) + tf.reduce_sum(logstd, axis=-1)
                for logstd, std, mean in zip(self._logstd, self._std, self._mean)
            )

    @property
    @Module.assert_scope_built
    def entropy(self):
        return self._entropy
