import abc

from ailib.modules import Module

__all__ = ['Distribution']


class Distribution(Module, metaclass=abc.ABCMeta):
    def __init__(self, name=None):
        super().__init__(name)

    @abc.abstractmethod
    def build(self, *args, **kwargs):
        pass

    @property
    @abc.abstractmethod
    def sample(self):
        pass

    @abc.abstractmethod
    def kl_distance(self, other):
        pass

    @abc.abstractmethod
    def neg_log(self, other):
        pass

    @property
    @abc.abstractmethod
    def entropy(self):
        pass
