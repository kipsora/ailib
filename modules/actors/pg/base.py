import abc

from ailib.modules import actors

__all__ = ['PGActor']


class PGActor(actors.Actor, metaclass=abc.ABCMeta):
    def __init__(self, env, name=None):
        super().__init__(env, name)
