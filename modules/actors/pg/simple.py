import tensorflow as tf

from ailib.modules import nn, Module
from ailib.modules.actors import pg
from ailib.utilities import spaces

__all__ = ['PGMLPActor']


class PGMLPActor(pg.PGActor):
    def __init__(self, env, network: nn.NNModule = None, name=None):
        super().__init__(env, name)

        self._act_space = self._env.action_space
        self._obs_space = self._env.observation_space
        assert isinstance(self._act_space, spaces.Box)

        if isinstance(self._obs_space, spaces.BasicSpace):
            self._network = network if network else nn.MLPModule(units=(300, 300, 300, 300, 300))
        else:
            assert isinstance(self._obs_space, spaces.HybridSpace)
            self._network = network if network else [nn.MLPModule(units=(300, 300, 300, 300, 300))
                                                     for _ in self._obs_space.spaces]

    @Module.in_scope_wrapper
    def apply(self, obs):
        if isinstance(self._obs_space, spaces.BasicSpace):
            x = self._network(obs)
        else:
            x = tf.concat([network(o) for network, o in zip(self._network, obs)], axis=1)
        return x

    @property
    def perturbable_variables(self):
        return tuple(filter(lambda v: 'LayerNorm' not in v.name, self.trainable_variables))
