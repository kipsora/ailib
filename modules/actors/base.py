import abc
import copy

from ailib.modules import Module
from ailib.utilities import environments, spaces

__all__ = ['Actor']


class Actor(Module, metaclass=abc.ABCMeta):
    def __init__(self, env: environments.Environment, name=None):
        super().__init__(name)

        self._env = env

    @property
    @Module.assert_scope_built
    def kernel_variables(self):
        return tuple(filter(lambda v: 'kernel' in v.name, self.global_variables))

    @property
    @abc.abstractmethod
    def perturbable_variables(self):
        """
        Property of an actor containing the variables that are able to be noised
        :return: list of variables that are able to be noised
        """
        pass

    def target_copy(self, prefix='Target'):
        result = copy.copy(self)
        result._name = prefix + '/' + self._name
        return result

    @property
    def state_space(self) -> spaces.Space:
        return None

    @property
    def need_step(self):
        return False
