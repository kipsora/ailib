import copy

import numpy
import tensorflow as tf

from ailib.modules import actors, nn, Module
from ailib.utilities import environments, spaces

__all__ = ['MLPActor', 'MLPSteppingActor']


class MLPActor(actors.Actor):
    def __init__(self,
                 env: environments.Environment,
                 network: nn.MLPModule=None,
                 name=None):
        super().__init__(env, name)

        self._act_space = self._env.action_space
        self._obs_space = self._env.observation_space
        assert isinstance(self._act_space, spaces.Box)

        if isinstance(self._obs_space, spaces.BasicSpace):
            self._network = network if network else nn.MLPModule(units=(300, 300, 300))
        else:
            assert isinstance(self._obs_space, spaces.HybridSpace)
            self._network = network if network else [nn.MLPModule(units=(300, 300, 300))
                                                     for _ in self._obs_space.spaces]

    def target_copy(self, prefix='Target'):
        result = super(MLPActor, self).target_copy(prefix)
        if isinstance(self._obs_space, spaces.HybridSpace):
            result._network = [copy.copy(network) for network in self._network]
        return result

    @Module.in_scope_wrapper
    def apply(self, obs):
        if isinstance(self._obs_space, spaces.BasicSpace):
            x = self._network(obs)
        else:
            x = tf.concat([network(o) for network, o in zip(self._network, obs)], axis=1)
        x = tf.layers.dense(x, units=numpy.prod(self._act_space.shape))
        x = tf.nn.tanh(x)
        x = tf.reshape(x, shape=(-1,) + self._act_space.shape)
        return x

    @property
    @Module.assert_scope_built
    def perturbable_variables(self):
        return tuple(filter(lambda v: 'LayerNorm' not in v.name, self.trainable_variables))


class MLPSteppingActor(actors.Actor):
    def __init__(self,
                 env: environments.Environment,
                 network: nn.MLPModule=None,
                 name=None):
        super().__init__(env, name)

        self._act_space = self._env.action_space
        self._obs_space = self._env.observation_space
        assert isinstance(self._act_space, spaces.Box)
        assert self._act_space.shape == spaces.SCALAR_SHAPE

        if isinstance(self._obs_space, spaces.BasicSpace):
            self._network = network if network else nn.MLPModule(units=(1024, 1024, 512, 512, 256))
        else:
            assert isinstance(self._obs_space, spaces.HybridSpace)
            self._network = network if network else [nn.MLPModule(units=(1024, 1024, 512, 512, 256))
                                                     for _ in self._obs_space.spaces]
        assert isinstance(env, environments.wrappers.StepByStepWrapper)
        self._last_layers = [tf.layers.Dense(units=1, activation=tf.tanh) for _ in range(env.total_steps)]

    def target_copy(self, prefix='Target'):
        result = super(MLPSteppingActor, self).target_copy(prefix)
        if isinstance(self._obs_space, spaces.HybridSpace):
            result._network = [copy.copy(network) for network in self._network]
        return result

    @Module.in_scope_wrapper
    def apply(self, obs, step):
        if isinstance(self._obs_space, spaces.BasicSpace):
            x = self._network(obs)
        else:
            x = tf.concat([network(o) for network, o in zip(self._network, obs)], axis=1)
        step = tf.one_hot(step, self._env.total_steps)
        x = tf.reduce_sum(tf.concat([layer(x) for layer in self._last_layers], axis=1) * step, axis=1)
        x = tf.reshape(x, shape=(-1,))
        return x

    @property
    @Module.assert_scope_built
    def perturbable_variables(self):
        return tuple(filter(lambda v: 'LayerNorm' not in v.name, self.trainable_variables))

    @property
    def need_step(self):
        return True
