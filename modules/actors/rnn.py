import numpy as np

import tensorflow as tf

from ailib.modules import actors, Module, nn
from ailib.utilities import spaces

__all__ = ['FixedShapeRNNJointActor', 'FixedShapeRNNSteppingActor']


class FixedShapeRNNJointActor(actors.Actor):
    def __init__(self, env, encoder=None, decoder=None, name=None):
        super().__init__(env, name)
        assert isinstance(self._env.observation_space, spaces.Box)

        self._encoder = encoder if encoder else nn.rnn.FixedShapeStaticLSTMModule(units=256, layers=3)
        self._decoder = decoder if decoder else nn.rnn.AutoAggressiveLSTMModule(loops=env.rnn_loops)

    @Module.in_scope_wrapper
    def apply(self, obs):
        obs = self._env.rnn_preprocessor(obs)
        with tf.variable_scope('Encoder'):
            _, encoder_state = self._encoder(obs)

        with tf.variable_scope('Decoder'):
            decoder_outputs, _ = self._decoder(encoder_state)

        decoder_outputs = self._env.rnn_postprocessor(decoder_outputs)
        decoder_outputs = tf.nn.tanh(decoder_outputs)

        return decoder_outputs

    @property
    @Module.assert_scope_built
    def perturbable_variables(self):
        return self.trainable_variables


class FixedShapeRNNSteppingActor(actors.Actor):
    def __init__(self, env, network: nn.rnn.StatefulLSTMModule = None, name=None):
        super().__init__(env, name)

        self._obs_space = self._env.observation_space
        self._act_space = self._env.action_space
        assert self._act_space.shape == spaces.SCALAR_SHAPE
        assert isinstance(self._act_space, spaces.Box)
        self._network = network if network else nn.rnn.StatefulLSTMModule(units=[256, 128, 256], layers=3)
        self._last_layers = [tf.layers.Dense(units=1, activation=tf.tanh) for _ in range(self._env.total_steps)]

    @property
    def perturbable_variables(self):
        return self.trainable_variables

    @Module.in_scope_wrapper
    def build(self):
        if not self._network.scope_built:
            self._network.build()

    @Module.in_scope_wrapper
    def apply(self, obs, state, step):
        if not self._network.scope_built:
            self._network.build()
        if isinstance(self._obs_space, spaces.BasicSpace):
            x = obs
        else:
            x = tf.concat([o for o in obs], axis=1)
        output, new_state = self._network(x, state)

        step = tf.one_hot(step, self._env.total_steps)
        x = tf.reduce_sum(tf.concat([layer(x) for layer in self._last_layers], axis=1) * step, axis=1)
        x = tf.reshape(x, shape=(-1,))

        return x, new_state

    @property
    def need_step(self):
        return True

    @property
    @Module.assert_scope_built
    def state_space(self) -> spaces.Space:
        return self._network.state_space
