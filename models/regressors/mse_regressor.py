import numpy

import tensorflow as tf

from ailib import models
from ailib.modules import nn
from ailib.utilities import helpers

__all__ = ['MSERegressor']


class MSERegressor(models.Model):
    def __init__(self, input_shape, output_shape, network=None, session=None, logger=None, name=None):
        import tensorflow.contrib as tc
        super().__init__(session, logger, name)
        self._input_shape = helpers.convert_to_shape(input_shape)
        self._output_shape = helpers.convert_to_shape(output_shape)
        self._network = network if network else nn.MLPModule()
        with self._scope:
            self._input = tf.placeholder(tf.float32, shape=[None] + self._input_shape, name='input')
            self._answer = tf.placeholder(tf.float32, shape=[None] + self._output_shape, name='answer')
            self._lr = tf.placeholder(tf.float32, shape=(), name='learning_rate')
            self._reg = tf.placeholder(tf.float32, shape=(), name='reg')
            self._training = tf.placeholder(tf.bool, shape=(), name='training')

            self._network.set_scope()
            self._output = self._network(tf.layers.flatten(self._input))
            self._output = tf.layers.dense(self._output, units=numpy.prod(self._output_shape))
            self._output = tf.reshape(self._output, (-1,) + self._output_shape)

            self._regression_loss = tf.reduce_mean(tf.square(self._answer - self._output))
            self._regularize_loss = tc.layers.apply_regularization(
                tc.layers.l2_regularizer(self._reg),
                weights_list=self._network.regularize_variables)
            self._loss = self._regression_loss + self._regularize_loss

            self._optimizer = tf.train.AdamOptimizer(learning_rate=self._lr)
            self._op_train = self._optimizer.minimize(self._loss)

            self._gradients = tf.gradients(self._output, self._input)
        self.initialize()

    def initialize(self):
        self._session.run(tf.variables_initializer(self.global_variables))

    def train(self, x, y, reg=0, learning_rate=1e-3):
        self._session.run(self._op_train, feed_dict={
            self._input: x,
            self._answer: y,
            self._reg: reg,
            self._lr: learning_rate,
            self._training: True
        })
        return self._session.run([self._regression_loss, self._regularize_loss, self._loss], feed_dict={
            self._input: x,
            self._answer: y,
            self._reg: reg,
            self._training: False
        })

    def test(self, x):
        return self.batch_test([x])[0]

    def batch_test(self, x):
        return self._session.run(self._output, feed_dict={
            self._input: x,
            self._training: False
        })

    def gradients(self, x):
        return self._session.run(self._gradients, feed_dict={
            self._input: [x],
            self._training: False
        })[0][0]
