import abc
import os

import collections
import tensorflow as tf

from ailib.utilities import helpers, spaces

__all__ = ['Model']


class Model(object, metaclass=abc.ABCMeta):
    def __init__(self, session=None, logger=None, name=None):
        self._name = name if name else self.__class__.__name__
        self._scope = tf.variable_scope(self._name, reuse=tf.AUTO_REUSE)
        self._abs_scope_name = helpers.get_tf_abs_scope_name(self._name)
        self._session = session if session else helpers.get_tf_session()
        self._logger = logger if logger else helpers.get_logger(self._name)

    @property
    def scope(self):
        return self._scope

    @property
    def name(self):
        return self._name

    @property
    def scope_name(self):
        return self._abs_scope_name

    @property
    def global_variables(self):
        return tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self._abs_scope_name)

    @property
    def saveable_objects(self):
        return tf.get_collection(tf.GraphKeys.SAVEABLE_OBJECTS, self._abs_scope_name)

    @property
    def session(self):
        return self._session

    @property
    def logger(self):
        return self._logger

    def __repr__(self):
        return self._name

    def save(self, path, step):
        path = os.path.abspath(path)
        if not os.path.exists(path):
            os.makedirs(path)
        saver = tf.train.Saver(self.global_variables + self.saveable_objects)
        saver.save(self._session, path + '/' + self._name, global_step=step, write_meta_graph=False)
        return path + '/' + self._name + '-' + str(step)

    def restore(self, path, step):
        path = os.path.abspath(path) + '/' + self._name + '-' + str(step)
        saver = tf.train.Saver(self.global_variables + self.saveable_objects)
        saver.restore(self._session, path)

    @staticmethod
    def _get_placeholder_from_space(space: spaces.Space, names=None):
        if isinstance(space, spaces.BasicSpace):
            if isinstance(names, str):
                return tf.placeholder(space.dtype, shape=(None,) + space.shape, name=names.format(''))
            elif names is None:
                return tf.placeholder(space.dtype, shape=(None,) + space.shape)
            else:
                raise NotImplementedError
        else:
            assert isinstance(space, spaces.HybridSpace)
            if isinstance(names, str):
                return tuple(tf.placeholder(dtype, shape=(None,) + shape, name=names.format(f'_{i}'))
                             for i, (dtype, shape) in enumerate(zip(space.dtypes, space.shapes)))
            elif isinstance(names, collections.Iterable):
                return tuple(tf.placeholder(dtype, shape=(None,) + shape, name=name)
                             for dtype, shape, name in zip(space.dtypes, space.shapes, names))
            elif names is None:
                return tuple(tf.placeholder(dtype, shape=(None,) + shape)
                             for dtype, shape in zip(space.dtypes, space.shapes))
            else:
                raise NotImplementedError

    @staticmethod
    def _get_single_value_dict_from_space(space: spaces.Space, placeholder, value):
        if space is None:
            return {}
        assert placeholder is not None and value is not None
        if isinstance(space, spaces.BasicSpace):
            return {placeholder: [value]}
        else:
            assert isinstance(space, spaces.HybridSpace)
            assert len(tuple(space.spaces)) == len(tuple(placeholder)) and len(tuple(value)) == len(tuple(space.spaces))
            return {k: [v] for k, v in zip(placeholder, value)}

    @staticmethod
    def _get_value_dict_from_space(space: spaces.Space, placeholder, value):
        if space is None:
            return {}
        assert placeholder is not None and value is not None
        if isinstance(space, spaces.BasicSpace):
            return {placeholder: value}
        else:
            assert isinstance(space, spaces.HybridSpace)
            assert len(tuple(space.spaces)) == len(tuple(placeholder)) and len(tuple(value)) == len(tuple(space.spaces))
            return {k: v for k, v in zip(placeholder, value)}

    @staticmethod
    def _get_tf_clipped_value_from_space(space: spaces.Space, value: tf.Tensor, names=None):
        if isinstance(space, spaces.BasicSpace):
            if isinstance(names, str):
                names = names.format('')
            elif isinstance(names, collections.Iterable):
                names = tuple(names)
                assert len(names) == 1
                names = names[0]
            elif names is not None:
                raise NotImplementedError
            if isinstance(space, spaces.Box):
                return tf.clip_by_value(value, *space.bound, name=names)
            else:
                return value
        else:
            assert isinstance(space, spaces.HybridSpace)
            if isinstance(names, str):
                return tuple(tf.clip_by_value(o, *space.bound, name=names.format(f'_{i}'))
                             if isinstance(space, spaces.Box) else o
                             for i, (o, space) in enumerate(zip(value, space.spaces)))
            elif isinstance(names, collections.Iterable):
                return tuple(tf.clip_by_value(o, *s.bound, name=name)
                             if isinstance(s, spaces.Box) else o
                             for o, s, name in zip(value, space.spaces, names))
            elif names is None:
                return tuple(tf.clip_by_value(o, *s.bound)
                             if isinstance(s, spaces.Box) else o
                             for o, s in zip(value, space.spaces))
            else:
                raise NotImplementedError
