import logging

import numpy
import tensorflow as tf

from ailib import models
from ailib.utilities import spaces

__all__ = ['Random']


class Random(models.Model):
    def __init__(self,
                 act_space: spaces.Box,
                 logger: logging.Logger = None,
                 session: tf.Session = None,
                 name=None):
        """
        This is an implementation of random policy.
        :param act_space: The desired action space
        :param logger: A logger instance to display message which will be created automatically if not provided
        :param session: A tensorflow session instance which will be created automatically if not provided
        :param name: The scope name of this model
        """
        super().__init__(session, logger, name)

        self._act_space = act_space
        self._min_act_space, self._max_act_space = act_space.bound

    def act(self):
        return numpy.random.rand(*self._act_space.shape) * (
                self._max_act_space - self._min_act_space) + self._min_act_space
