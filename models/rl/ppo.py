import tensorflow as tf

from ailib.models import rl
from ailib.modules import actors, normalizers, distributions
from ailib.utilities import environments, helpers, spaces

__all__ = ['PPO']


class PPO(rl.RLModel):
    def _build_action(self, entropy_coefficient, value_coefficient):
        assert isinstance(self._act_space, spaces.Box)
        with tf.variable_scope('ActionGenerator'):
            obs = self._obs = self._get_placeholder_from_space(self._obs_space, names='obs{}')
            if self._obs_norm:
                assert isinstance(self._obs_norm, normalizers.Normalizer)
                self._obs_norm.build()
                obs = self._obs_norm.normalize(obs)
                self._op_train_obs_norm = self._obs_norm.get_update_op(self._obs)
            obs = self._get_tf_clipped_value_from_space(self._obs_space, obs, 'clip_obs0{}')

            self._action = self._get_placeholder_from_space(self._act_space, names='act{}')

            self._clip_range = tf.placeholder(tf.float32, name='clip_range')

            self._actor_latent = self._actor.apply(obs)
            self._actor_pdf.build(self._actor_latent)
            self._norm_actor_action = self._actor_pdf.sample
            self._actor_self_neg_log = self._actor_pdf.neg_log(self._norm_actor_action)
            self._actor_action = tf.clip_by_value(
                helpers.denormalize_with_bound(self._norm_actor_action, self._act_space.bound),
                self._act_space.min_bound, self._act_space.max_bound
            )

            self._old_actor_value = tf.placeholder(tf.float32, shape=[None], name='old_actor_value')
            self._old_actor_neg_log = tf.placeholder(tf.float32, shape=[None], name='old_actor_neg_log')

            self._advantage = tf.placeholder(tf.float32, shape=[None], name='advantage')
            advantage_mean = tf.reduce_mean(self._advantage)
            advantage_sqr_mean = tf.reduce_mean(tf.square(self._advantage))
            advantage_std = tf.sqrt(advantage_sqr_mean - tf.square(advantage_mean))
            advantage = (self._advantage - advantage_mean) / (advantage_std + 1e-8)

            self._return = self._advantage + self._old_actor_value

            self._norm_action = helpers.normalize_with_bound(self._action, self._act_space.bound)

            self._actor_value = tf.reshape(tf.layers.dense(self._actor_latent, units=1), shape=[-1])
            self._clipped_actor_value = self._old_actor_value + tf.clip_by_value(
                self._actor_value - self._old_actor_value,
                -self._clip_range, self._clip_range
            )
            self._value_loss = 0.5 * tf.reduce_mean(tf.maximum(
                tf.square(self._clipped_actor_value - self._return),
                tf.square(self._actor_value - self._return)
            ))

            self._actor_action_neg_log = self._actor_pdf.neg_log(self._norm_action)
            ratio = tf.exp(self._old_actor_neg_log - self._actor_action_neg_log)
            self._pg_loss = -tf.reduce_mean(tf.minimum(
                advantage * ratio,
                advantage * tf.clip_by_value(ratio, 1 - self._clip_range, 1 + self._clip_range)
            ))
            self._entropy = self._actor_pdf.entropy
            self._loss = self._pg_loss - self._entropy * entropy_coefficient + self._value_loss * value_coefficient

    def _build_optimizer(self, optimizer, max_grad_norm):
        if max_grad_norm:
            raise NotImplementedError('Gradient clipping is not implemented')
        with tf.variable_scope('Optimizer'):
            self._learning_rate = tf.placeholder(tf.float32, name='lr')
            optimizer = optimizer(learning_rate=self._learning_rate)
            self._op_train = optimizer.minimize(self._loss)

    def __init__(self,
                 env: environments.Environment,
                 act_pdf: distributions.Distribution = None,
                 actor: actors.pg.PGActor = None,
                 optimizer=tf.train.RMSPropOptimizer,
                 use_obs_norm=True,
                 entropy_coefficient=0.01,
                 value_coefficient=0.5,
                 max_grad_norm=None,
                 session=None,
                 logger=None,
                 name=None):
        """
        Implementation of Proximity Policy Optimization
        :param env: The environment PPO will act on
        :param act_pdf: The probability distribution function of actor
        :param optimizer: The instance of the optimizer
        :param use_obs_norm: Whether to use normalizer of observation
        :param session: A tensorflow session instance which will be created automatically if not provided
        :param logger: A logger instance to display message which will be created automatically if not provided
        :param name: The scope name of this model
        """
        super().__init__(session, logger, name)

        self._obs_space = env.observation_space
        self._act_space = env.action_space
        assert isinstance(self._act_space, spaces.Box)

        self._actor = actor if actor else actors.pg.PGMLPActor(env)

        self._actor_pdf = act_pdf if act_pdf else distributions.DiagonalGaussian(self._act_space)

        self._obs_norm = normalizers.RunningMeanNormalizer(self._obs_space) if use_obs_norm else None

        with tf.variable_scope(self._name):
            self._build_action(entropy_coefficient, value_coefficient)
            self._build_optimizer(optimizer, max_grad_norm)

        abs_name = helpers.get_tf_abs_scope_name(self._name)
        model_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=abs_name)
        self._session.run(tf.variables_initializer(model_variables))

    def act(self, obs):
        feed_dict = self._get_single_value_dict_from_space(self._obs_space, self._obs, obs)
        if self._obs_norm:
            self._session.run(self._op_train_obs_norm, feed_dict=feed_dict)
        action, value = self._session.run([self._actor_action, [self._actor_value, self._actor_self_neg_log]],
                                          feed_dict=feed_dict)
        return action[0], (value[0][0], value[1][0])

    def train(self, obs, action, advantage, old_value, old_neg_log, clip_range=0.2, lr=1e-4):
        feed_dict = {
            self._old_actor_value: old_value,
            self._old_actor_neg_log: old_neg_log,
            self._learning_rate: lr,
            self._action: action,
            self._clip_range: clip_range,
            self._advantage: advantage
        }
        feed_dict.update(self._get_value_dict_from_space(self._obs_space, self._obs, obs))
        _, loss = self._session.run([self._op_train, [self._loss, self._pg_loss, self._value_loss, self._entropy]],
                                    feed_dict=feed_dict)
        return loss

    @property
    def memory_entries(self) -> dict:
        result = {
            'obs': self._obs_space,
            'action': self._act_space,
            'advantage': spaces.Box(spaces.SCALAR_SHAPE),
            'old_value': spaces.Box(spaces.SCALAR_SHAPE),
            'old_neg_log': spaces.Box(spaces.SCALAR_SHAPE)
        }
        return result

    @property
    def observation_normalizer(self):
        assert isinstance(self._obs_norm, normalizers.Normalizer)
        return self._obs_norm

    def update_observation_normalizer(self, obs):
        assert self._obs_norm
        feed_dict = self._get_single_value_dict_from_space(self._obs_space, self._obs, obs)
        self._session.run(self._op_train_obs_norm, feed_dict=feed_dict)
