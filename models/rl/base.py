import abc
import logging
import tensorflow as tf

from ailib import models

__all__ = ['RLModel']


class RLModel(models.Model):
    def __init__(self, session: tf.Session = None, logger: logging.Logger = None, name=None):
        super().__init__(session, logger, name)

    @abc.abstractmethod
    def act(self, *args, **kwargs):
        pass

    @property
    @abc.abstractmethod
    def memory_entries(self) -> dict:
        pass
