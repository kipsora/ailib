import logging

import tensorflow as tf

from ailib.models import rl
from ailib.modules import normalizers, critics, actors
from ailib.utilities import helpers, environments, spaces

__all__ = ['DeepDPG']


class DeepDPG(rl.RLModel):
    @staticmethod
    def __apply_actor(actor: actors.Actor, obs, state=None, step=None, action_only=False):
        actor_apply_parameters = {'obs': obs}
        if state is not None:
            actor_apply_parameters.setdefault('state', state)
        if step is not None:
            actor_apply_parameters.setdefault('step', step)
        actor_apply_results = actor.apply(**actor_apply_parameters)
        if action_only:
            return actor_apply_results[0] if state else actor_apply_results
        else:
            return actor_apply_results if state else actor_apply_results, None

    def _build_action(self, gamma):
        assert isinstance(self._act_space, spaces.Box)
        with tf.variable_scope('ActionGenerator'):
            self._batch_size = tf.placeholder(tf.int32, name='batch_size')
            obs0 = self._obs0 = self._get_placeholder_from_space(self._obs_space, 'obs0{}')
            obs1 = self._obs1 = self._get_placeholder_from_space(self._obs_space, 'obs1{}')
            if isinstance(self._actor, actors.FixedShapeRNNSteppingActor):
                self._actor.build()
            self._state0 = self._get_placeholder_from_space(self._actor.state_space, 'state0{}') \
                if self._actor.state_space else None
            self._step0 = tf.placeholder(tf.int32, shape=(None,), name='step0') if self._actor.need_step else None

            if self._obs_norm:
                assert isinstance(self._obs_norm, normalizers.Normalizer)
                self._obs_norm.build()
                obs0 = self._obs_norm.normalize(obs0)
                obs1 = self._obs_norm.normalize(obs1)
                self._op_train_obs_norm = self._obs_norm.get_update_op(self._obs0)
            obs0 = self._get_tf_clipped_value_from_space(self._obs_space, obs0, 'clip_obs0{}')
            obs1 = self._get_tf_clipped_value_from_space(self._obs_space, obs1, 'clip_obs1{}')

            self._tmp_norm_obs0 = obs0

            self._action = self._get_placeholder_from_space(self._act_space, names='action{}')
            self._reward = tf.placeholder(tf.float32, shape=[None], name='reward')
            self._terminate = tf.placeholder(tf.bool, shape=[None], name='terminal')
            self._tau = tf.placeholder(tf.float32, name='tau')

            self._norm_action = helpers.normalize_with_bound(self._action, self._act_space.bound)

            self._norm_actor_action, self._actor_state = self.__apply_actor(
                self._actor, obs0, self._state0, self._step0)

            if self._enable_popart or self._val_norm:
                if not self._val_norm:
                    self._val_norm = normalizers.RunningMeanNormalizer(spaces.Box([1]))
                else:
                    assert isinstance(self._val_norm, normalizers.Normalizer)
                self._val_norm.build()

                self._norm_critic_val = self._critic.apply(obs0, self._norm_action)
                self._norm_critic_actor_val = self._critic.apply(obs0, self._norm_actor_action, reuse=True)

                self._critic_val = self._val_norm.denormalize(self._norm_critic_val)
                self._critic_actor_val = self._val_norm.denormalize(self._norm_critic_actor_val)
                self._target = tf.where(
                    self._terminate, self._reward,
                    self._reward + gamma * self._val_norm.denormalize(
                        self._target_critic.apply(
                            obs1,
                            self.__apply_actor(self._target_actor, obs1, self._actor_state,
                                               self._step0 + 1 if self._step0 is not None else None,
                                               action_only=True)
                        )
                    ),
                    name='target'
                )
                self._norm_target = self._val_norm.normalize(self._target)
                self._op_train_val_normalizer = self._val_norm.get_update_op(tf.expand_dims(self._target, axis=1))

                self._critic_loss = tf.reduce_mean(tf.square(self._norm_target - self._norm_critic_val),
                                                   name='critic_loss')
                self._actor_loss = tf.reduce_mean(-self._critic_actor_val, name='actor_loss')
            else:
                self._critic_val = self._critic.apply(obs0, self._norm_action)
                self._critic_actor_val = self._critic.apply(obs0, self._norm_actor_action, reuse=True)
                self._target = tf.where(self._terminate, self._reward, self._reward + gamma *
                                        self._target_critic.apply(
                                            obs1,
                                            self.__apply_actor(self._target_actor, obs1, self._actor_state,
                                                               self._step0 + 1 if self._step0 is not None else None,
                                                               action_only=True)
                                        ),
                                        name='target')
                self._critic_loss = tf.reduce_mean(tf.square(self._target - self._critic_val), name='ciritc_loss')
                self._actor_loss = tf.reduce_mean(-self._critic_actor_val, name='actor_loss')

            # Defining noisy actor operations
            assert isinstance(self._act_space, spaces.Box)
            self._action_noise = tf.placeholder(tf.float32, name='action_noise')

            if self._enable_param_noise:
                # See https://arxiv.org/abs/1706.01905
                self._param_noise_std = tf.placeholder(tf.float32, name='param_noise_std')
                self._param_noise_actor = self._actor.target_copy('ParamNoiseActor')

                norm_param_noise_actor_action = self.__apply_actor(
                    self._param_noise_actor, obs0, self._state0, self._step0, action_only=True)
                norm_noise_actor_action = (tf.where(
                    tf.random_uniform((self._batch_size,) + self._act_space.shape) < self._action_noise,
                    tf.random_uniform((self._batch_size,) + self._act_space.shape) * 2 - 1,
                    norm_param_noise_actor_action
                ) + norm_param_noise_actor_action) / 2
                self._critic_noise_actor_val = self._critic.apply(obs0, norm_noise_actor_action, reuse=True)
                if self._val_norm:
                    self._critic_noise_actor_val = self._val_norm.denormalize(self._critic_noise_actor_val)
                self._noise_actor_action = helpers.denormalize_with_bound(
                    norm_noise_actor_action, self._act_space.bound)

                self._op_move_noise_actor = [
                    tf.assign(n, s + tf.random_normal(tf.shape(s)) * self._param_noise_std)
                    if s in self._actor.perturbable_variables else tf.assign(n, s)
                    for n, s in zip(self._param_noise_actor.global_variables, self._actor.global_variables)
                ]

                self._adaptive_param_noise_actor = self._actor.target_copy('AdaptiveParamNoiseActor')
                norm_adaptive_param_noise_actor_action = self.__apply_actor(
                    self._adaptive_param_noise_actor, obs0, self._state0, self._step0, action_only=True)
                self._op_move_adaptive_noise_actor = [
                    tf.assign(n, s + tf.random_normal(tf.shape(s)) * self._param_noise_std)
                    if s in self._actor.perturbable_variables else tf.assign(n, s)
                    for n, s in zip(self._adaptive_param_noise_actor.global_variables, self._actor.global_variables)
                ]

                self._adaptive_policy_distance = tf.reduce_mean(tf.square(
                    norm_adaptive_param_noise_actor_action - self._norm_actor_action))

                self._logger.debug('Noise Actor Variables: {}'.format(
                    [v.name for v in self._param_noise_actor.global_variables]))
                self._logger.debug('Adaptive Noise Actor Variables: {}'.format([
                    v.name for v in self._adaptive_param_noise_actor.global_variables]))
            else:
                norm_noise_actor_action = (tf.where(
                    tf.random_uniform((self._batch_size,) + self._act_space.shape) <= self._action_noise,
                    tf.random_uniform((self._batch_size,) + self._act_space.shape) * 2 - 1,
                    self._norm_actor_action
                ) + self._norm_actor_action) / 2
                self._critic_noise_actor_val = self._critic.apply(obs0, norm_noise_actor_action, reuse=True)
                if self._val_norm:
                    self._critic_noise_actor_val = self._val_norm.denormalize(self._critic_noise_actor_val)
                self._noise_actor_action = helpers.denormalize_with_bound(
                    norm_noise_actor_action, self._act_space.bound, name='noise_actor_action')

            with tf.variable_scope('Target/UpdateOP'):
                self._op_init_target_actor = [tf.assign(t, s) for t, s in zip(
                    self._target_actor.global_variables, self._actor.global_variables)]
                self._op_init_target_critic = [tf.assign(t, s) for t, s in zip(
                    self._target_critic.global_variables, self._critic.global_variables)]
                self._op_move_target_actor = [tf.assign(t, s * self._tau + t * (1 - self._tau)) for t, s in zip(
                    self._target_actor.global_variables, self._actor.global_variables)]
                self._op_move_target_critic = [tf.assign(t, s * self._tau + t * (1 - self._tau)) for t, s in zip(
                    self._target_critic.global_variables, self._critic.global_variables)]

    def _build_optimizer(self, critic_optimizer, actor_optimizer):
        with tf.variable_scope('Optimizer'):
            self._critic_lr = tf.placeholder(tf.float32, name='critic_lr')
            self._actor_lr = tf.placeholder(tf.float32, name='actor_lr')

            self._op_train_critic = critic_optimizer(learning_rate=self._critic_lr,
                                                     name='critic_optimizer').minimize(
                self._critic_loss, var_list=self._critic.trainable_variables, name='critic_optimizer_gradient')
            self._op_train_actor = actor_optimizer(learning_rate=self._actor_lr, name='actor_optimizer').minimize(
                self._actor_loss, var_list=self._actor.trainable_variables, name='actor_optimizer_gradient')

    def _build_popart(self):
        if self._enable_popart:
            # See https://arxiv.org/abs/1602.07714
            # It is better to combine critic value optimization with POP-ART which can be compensated for the bias
            # brought by the value normalization
            with tf.variable_scope('POPART'):
                assert isinstance(self._val_norm, normalizers.RunningMeanNormalizer)
                assert hasattr(self._critic, 'popart_variables')
                self._old_val_norm_mean = tf.placeholder(tf.float32, shape=[1], name='old_val_normalizer_mean')
                self._old_val_norm_std = tf.placeholder(tf.float32, shape=[1], name='old_val_normalizer_std')
                self._op_popart = []
                for i in (self._critic, self._target_critic):
                    k, b = i.popart_variables
                    self._op_popart.append(tf.assign(k, k * self._old_val_norm_std / self._val_norm.std))
                    self._op_popart.append(tf.assign(b, (b * self._old_val_norm_std + self._old_val_norm_mean -
                                                         self._val_norm.mean) / self._val_norm.std))

    def __init__(self,
                 env: environments.Environment,
                 actor: actors.Actor = None,
                 critic: critics.Critic = None,
                 actor_optimizer=tf.train.AdamOptimizer,
                 critic_optimizer=tf.train.AdamOptimizer,
                 use_obs_norm=True,
                 use_val_norm=True,
                 gamma=0.9,
                 enable_popart=False,
                 enable_param_noise=False,
                 session: tf.Session = None,
                 logger: logging.Logger = None,
                 name=None):
        """
        This is an implementation of Deep Deterministic Policy Gradient for continuous controlling.
        :param env: The environment DeepDPG will act on
        :param actor: The class of the Actor which will be built
        :param critic: The class of the Critic which will be built
        :param actor_optimizer: The optimizer for the actor
        :param critic_optimizer: The optimizer for the critic
        :param use_obs_norm: Whether use normalizer of observation
        :param use_val_norm: Whether use normalizer of critic value
        :param gamma: The decay factor of reinforcement learning
        :param enable_popart: Whether use POP-ART or not
        :param logger: A logger instance to display message which will be created automatically if not provided
        :param session: A tensorflow session instance which will be created automatically if not provided
        :param name: The scope name of this model
        """
        super().__init__(session, logger, name)

        self._obs_space = env.observation_space
        self._act_space = env.action_space

        assert isinstance(self._act_space, spaces.Box)

        self._actor = actor if actor else actors.MLPActor(env)
        self._critic = critic if critic else critics.SimpleCritic(env)
        self._target_actor = self._actor.target_copy()
        self._target_critic = self._critic.target_copy()

        self._enable_popart = enable_popart
        self._enable_param_noise = enable_param_noise

        self._val_norm = normalizers.RunningMeanNormalizer(spaces.Box([1])) if use_val_norm else None
        self._obs_norm = normalizers.RunningMeanNormalizer(self._obs_space) if use_obs_norm else None

        with tf.variable_scope(self._name):
            self._build_action(gamma)
            self._build_optimizer(critic_optimizer, actor_optimizer)
            self._build_popart()

        abs_name = helpers.get_tf_abs_scope_name(self._name)
        model_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=abs_name)
        self._session.run(tf.variables_initializer(model_variables))
        self._session.run((self._op_init_target_actor, self._op_init_target_critic))

    def train(self, obs0, obs1, action, reward, terminate,
              state=None, step=None, actor_lr=1e-4, critic_lr=1e-4, tau=1e-2):
        feed_dict = {
            self._action: action,
            self._reward: reward,
            self._terminate: terminate,
            self._actor_lr: actor_lr,
            self._critic_lr: critic_lr,
            self._batch_size: len(terminate)
        }
        feed_dict.update(self._get_value_dict_from_space(self._obs_space, self._obs0, obs0))
        feed_dict.update(self._get_value_dict_from_space(self._obs_space, self._obs1, obs1))
        feed_dict.update(self._get_value_dict_from_space(self._actor.state_space, self._state0, state))
        if self._step0 is not None:
            feed_dict.setdefault(self._step0, step)

        if self._val_norm and self._enable_popart:
            old_val_norm_mean, old_val_norm_std = self._session.run((self._val_norm.mean, self._val_norm.std))
            self._session.run(self._op_train_val_normalizer, feed_dict=feed_dict)
            self._session.run(self._op_popart, feed_dict={
                self._old_val_norm_mean: old_val_norm_mean,
                self._old_val_norm_std: old_val_norm_std
            })
        elif self._val_norm:
            self._session.run(self._op_train_val_normalizer, feed_dict=feed_dict)
        _, critic_loss = self._session.run((self._op_train_critic, self._critic_loss), feed_dict=feed_dict)
        _, actor_loss = self._session.run((self._op_train_actor, self._actor_loss), feed_dict=feed_dict)
        self._session.run((self._op_move_target_critic, self._op_move_target_actor), feed_dict={self._tau: tau})
        return critic_loss, actor_loss

    def act(self, obs, action_noise_prob, state=None, step=None,
            need_critic_actor_value=False, need_critic_noise_actor_value=False):
        assert isinstance(self._act_space, spaces.Box)
        feed_dict = {
            self._action_noise: action_noise_prob,
            self._batch_size: 1
        }
        feed_dict.update(self._get_single_value_dict_from_space(self._obs_space, self._obs0, obs))
        feed_dict.update(self._get_single_value_dict_from_space(self._actor.state_space, self._state0, state))
        if self._step0 is not None:
            feed_dict.setdefault(self._step0, [step])

        need_tensor_list = [self._noise_actor_action]
        if self._actor_state:
            need_tensor_list.append(self._actor_state)
        if need_critic_actor_value:
            need_tensor_list.append(self._critic_actor_val)
        if need_critic_noise_actor_value:
            need_tensor_list.append(self._critic_noise_actor_val)
        values = self._session.run(need_tensor_list, feed_dict=feed_dict)
        results = []
        if need_critic_noise_actor_value:
            results.append(values.pop()[0])
        if need_critic_actor_value:
            results.append(values.pop()[0])
        if self._actor_state:
            state = values.pop()
            if isinstance(self._actor.state_space, spaces.BasicSpace):
                results.append(state[0])
            else:
                results.append(tuple(s[0] for s in state))
        results.append(values.pop()[0])
        return tuple(reversed(results))

    def update_observation_normalizer(self, obs):
        assert self._obs_norm
        feed_dict = self._get_single_value_dict_from_space(self._obs_space, self._obs0, obs)
        self._session.run(self._op_train_obs_norm, feed_dict=feed_dict)

    def get_param_noise_policy_distance(self, obs, param_noise_std, state=None, step=None):
        assert self._enable_param_noise
        self._session.run(self._op_move_adaptive_noise_actor, feed_dict={
            self._param_noise_std: param_noise_std
        })
        feed_dict = self._get_value_dict_from_space(self._obs_space, self._obs0, obs)
        feed_dict.update(self._get_value_dict_from_space(self._actor.state_space, self._state0, state))
        if self._step0 is not None:
            feed_dict.setdefault(self._step0, step)
        distance = self._session.run(self._adaptive_policy_distance, feed_dict=feed_dict)
        return distance

    def apply_param_noise(self, param_noise_std):
        assert self._enable_param_noise
        self._session.run(self._op_move_noise_actor, feed_dict={
            self._param_noise_std: param_noise_std
        })

    @property
    def value_normalizer(self):
        assert isinstance(self._val_norm, normalizers.RunningMeanNormalizer)
        return self._val_norm

    @property
    def observation_normalizer(self):
        assert isinstance(self._obs_norm, normalizers.Normalizer)
        return self._obs_norm

    @property
    def memory_entries(self) -> dict:
        result = {
            'obs0': self._obs_space,
            'obs1': self._obs_space,
            'action': self._act_space,
            'reward': spaces.Box(spaces.SCALAR_SHAPE),
            'terminate': spaces.Discrete([True, False], 'bool'),
        }
        if self._state0 is not None:
            result.setdefault('state', self._actor.state_space)
        if self._step0 is not None:
            result.setdefault('step', spaces.Box(spaces.SCALAR_SHAPE))
        return result

    @property
    def actor(self):
        return self._actor

    @property
    def critic(self):
        return self._critic
