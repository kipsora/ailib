import abc

__all__ = ['Dataset']


class Dataset(object, metaclass=abc.ABCMeta):
    def __init__(self):
        pass

    @abc.abstractmethod
    def sample(self, batch_size):
        pass
