import abc

import numpy

from ailib import datasets

__all__ = ['FunctionDataset', 'Gabor', 'Multi', 'MexicanHat2D', 'MexicanHat3D']


class FunctionDataset(datasets.Dataset, metaclass=abc.ABCMeta):
    def __init__(self):
        super().__init__()

    @property
    @abc.abstractmethod
    def feature_shape(self):
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def label_shape(self):
        raise NotImplementedError

    @abc.abstractmethod
    def __call__(self, x):
        pass

    def sample(self, batch_size):
        x = numpy.random.randn(batch_size, *self.feature_shape)
        return x, self(x)


class Gabor(FunctionDataset):
    def __init__(self):
        super().__init__()

    @property
    def feature_shape(self):
        return [2]

    @property
    def label_shape(self):
        return [1]

    def __call__(self, x):
        x0, x1 = x[:, 0], x[:, 1]
        return (0.5 * numpy.pi * numpy.exp(-2 * (numpy.square(x0) + numpy.square(
            x1))) * numpy.cos(2 * numpy.pi * (x0 + x1)))[:, numpy.newaxis]


class Multi(FunctionDataset):
    def __init__(self):
        super().__init__()

    @property
    def feature_shape(self):
        return [5]

    @property
    def label_shape(self):
        return [1]

    def __call__(self, x):
        x1, x2, x3, x4, x5 = x[:, 0], x[:, 1], x[:, 2], x[:, 3], x[:, 4]
        return (0.79 + 1.27 * x1 * x2 + 1.56 * x1 * x4 + 3.42 * x2 * x5 + 2.06 * x3 * x4 * x5)[:, numpy.newaxis]


class Linear(FunctionDataset):
    def __init__(self):
        super().__init__()

    @property
    def feature_shape(self):
        return [1]

    @property
    def label_shape(self):
        return [1]

    def __call__(self, x):
        return 5 * x + 4


class MexicanHat2D(FunctionDataset):
    def __init__(self):
        super().__init__()

    def __call__(self, x):
        return numpy.sin(numpy.abs(x)) / numpy.abs(x)

    @property
    def feature_shape(self):
        return [1]

    @property
    def label_shape(self):
        return [1]


class MexicanHat3D(FunctionDataset):
    def __init__(self):
        super().__init__()

    def __call__(self, x):
        f = numpy.sqrt(numpy.square(x[:, 0]) + numpy.square(x[:, 1]))
        return (numpy.sin(f) / f)[numpy.newaxis]

    @property
    def feature_shape(self):
        return [2]

    @property
    def label_shape(self):
        return [1]
