import abc


class Noise(object, metaclass=abc.ABCMeta):
    def __init__(self):
        pass

    @abc.abstractmethod
    def step(self, *args, **kwargs):
        pass

    @property
    @abc.abstractmethod
    def value(self):
        pass


class LinearNoise(Noise):
    def __init__(self, init_value, min_value, decay):
        super().__init__()
        self._init_value = init_value
        self._min_value = min_value
        self._decay = decay
        self._value = self._init_value

    def step(self):
        self._value = max(self._min_value, self._value - self._decay)
        return self._value

    @property
    def value(self):
        return self._value


class AdaptiveParamNoise(Noise):
    def step(self, distance):
        self._value = self._value / self._adoption if distance > self._threshold else self._value * self._adoption
        return self._value

    @property
    def value(self):
        return self._value

    def __init__(self, init_value, adoption, threshold):
        super().__init__()
        self._adoption = adoption
        self._threshold = threshold
        self._init_value = init_value
        self._value = self._init_value
