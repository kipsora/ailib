import numpy as np

from ailib import models
from ailib.utilities.memories import Memory


class RingMemory(Memory):
    def __init__(self, max_size: int, model: models.rl.RLModel):
        super().__init__(max_size, model)

        self._index = 0

    def store(self, **kwargs):
        place_index = self._index % self._max_size
        for k, current_buffer in self._buffer.items():
            v = kwargs[k]
            if isinstance(current_buffer, tuple):
                for buffer, value in zip(current_buffer, v):
                    buffer[place_index] = value
            else:
                assert isinstance(current_buffer, np.ndarray), current_buffer.__class__
                current_buffer[place_index] = v
        self._index += 1

    def batch(self, batch_size, columns=None, alias=None):
        indexes = np.random.choice(self.size, batch_size)
        return self.take(indexes, columns, alias)

    @property
    def size(self):
        return min(self._index, self._max_size)
