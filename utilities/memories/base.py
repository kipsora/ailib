import abc
import numpy as np

from ailib import models
from ailib.utilities import spaces

__all__ = ['Memory']


class Memory(object, metaclass=abc.ABCMeta):
    def __init__(self, max_size: int, model: models.rl.RLModel):
        self._buffer = dict()
        self._max_size = max_size
        for k, v in model.memory_entries.items():
            if isinstance(v, spaces.BasicSpace):
                self._buffer.setdefault(k, np.zeros(shape=[max_size] + list(v.shape), dtype=v.dtype))
            else:
                assert isinstance(v, spaces.HybridSpace)
                self._buffer.setdefault(k, tuple(np.zeros(shape=[max_size] + list(shape), dtype=dtype)
                                                 for shape, dtype in zip(v.shapes, v.dtypes)))

    @abc.abstractmethod
    def store(self, **kwargs):
        pass

    @abc.abstractmethod
    def batch(self, batch_size):
        pass

    @property
    @abc.abstractmethod
    def size(self):
        pass

    def take(self, indexes, columns, alias):
        if columns is not None:
            if alias is None:
                alias = columns
            values = {a: self._buffer[c] for c, a in zip(columns, alias)}
            return {
                k: tuple(i[indexes] for i in v) if isinstance(v, tuple) else v[indexes]
                for k, v in values.items()
            }
        else:
            return {
                k: tuple(i[indexes] for i in v) if isinstance(v, tuple) else v[indexes]
                for k, v in self._buffer.items()
            }

    def all(self, columns=None, alias=None, shuffle=True):
        indexes = np.arange(self.size)
        if shuffle:
            np.random.shuffle(indexes)
        return self.take(indexes, columns, alias)

    def all_batch(self, batch_size, columns=None, alias=None):
        assert self.size > 0
        indexes = np.arange(self.size)
        np.random.shuffle(indexes)
        for i in range(0, self.size, batch_size):
            yield self.take(indexes[i: i + batch_size], columns, alias)
