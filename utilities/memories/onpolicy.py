import numpy as np

from ailib import models
from ailib.utilities.memories import Memory

__all__ = ['OnPolicyMemory']


class OnPolicyMemory(Memory):
    def store(self, **kwargs):
        assert self._index is not None and self._index < self._max_size
        for k, current_buffer in self._buffer.items():
            value = kwargs[k]
            if isinstance(current_buffer, tuple):
                for buffer, value in zip(current_buffer, value):
                    buffer[self._index] = value
            else:
                assert isinstance(current_buffer, np.ndarray), current_buffer.__class__
                current_buffer[self._index] = value
        self._index += 1

    def batch(self, batch_size, columns=None, alias=None):
        indexes = np.random.choice(self.size, batch_size)
        return self.take(indexes, columns, alias)

    @property
    def size(self):
        return self._index

    def __init__(self, max_size: int, model: models.rl.RLModel):
        super().__init__(max_size, model)

        self._index = None

    def reset(self):
        assert self._index is None
        self._index = 0

    def close(self):
        assert self._index is not None
        self._index = None
