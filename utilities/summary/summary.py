import io
import tensorflow as tf
import matplotlib.pyplot as plt

from ailib.utilities import helpers

__all__ = ['Summary']


class Summary(object):
    def __init__(self, logdir, session: tf.Session=None):
        self._variables = {}
        self._summaries = []
        self._logdir = logdir
        self._session = session if session else helpers.get_tf_session()
        self._writer = None
        self._merged = None

        self._log_placeholder = tf.placeholder(tf.string, name='log')
        self._log = tf.summary.text('log', self._log_placeholder)

    def register(self, name, dtype, shape, method):
        assert self._merged is None
        assert name not in self._variables
        if method == 'image':
            self._variables.setdefault(name, tf.placeholder(dtype, helpers.convert_to_shape(shape), name=name))
        else:
            self._variables.setdefault(name, tf.placeholder(dtype, helpers.convert_to_shape(shape), name=name))
        if method == 'scalar':
            self._summaries.append(tf.summary.scalar(name, tf.reduce_mean(self._variables[name])))
        elif method == 'histogram':
            self._summaries.append(tf.summary.histogram(name, self._variables[name]))
        elif method == 'image':
            self._summaries.append(tf.summary.image(name, self._variables[name], max_outputs=1))
        else:
            raise NotImplementedError
        return self

    def register_tf_tensor(self, name, variable: tf.Tensor, method='scalar'):
        assert self._merged is None
        assert name not in self._variables
        self._variables.setdefault(name, variable)
        if method == 'scalar':
            self._summaries.append(tf.summary.scalar(name, tf.reduce_mean(variable)))
        elif method == 'histogram':
            self._summaries.append(tf.summary.histogram(name, variable))
        elif method == 'image':
            self._summaries.append(tf.summary.image(name, variable, max_outputs=1))
        else:
            raise NotImplementedError
        return self

    def register_scalar(self, name, dtype=tf.float32, shape=()):
        return self.register(name, dtype, shape, 'scalar')

    def register_histogram(self, name, dtype=tf.float32, shape=()):
        return self.register(name, dtype, shape, 'histogram')

    def register_scalar_tf_tensor(self, name, variable: tf.Tensor):
        return self.register_tf_tensor(name, variable, 'scalar')

    def register_histogram_tf_tensor(self, name, variable: tf.Tensor):
        return self.register_tf_tensor(name, variable, 'histogram')

    def register_image(self, name, dtype, height, width, channel):
        return self.register(name, dtype, (None, height, width, channel), 'image')

    def register_image_tf_tensor(self, name, variable: tf.Tensor):
        return self.register_tf_tensor(name, variable, 'image')

    def register_scalars_with_default(self, *names):
        for name in names:
            self.register_scalar(name)
        return self

    def summary(self, step, **kwargs):
        assert self._writer is not None
        if self._merged is None:
            self._merged = tf.summary.merge(self._summaries)
        merged = self._session.run(self._merged, feed_dict=dict(
            [(self._variables[k], kwargs[k]) for k in self._variables]))
        self._writer.add_summary(merged, global_step=step)

    def log(self, log, step=None, replace_with_html_tags=True):
        log = str(log)
        if replace_with_html_tags:
            log = log.replace('\n', '<br>').replace('    ', '&nbsp;&nbsp;')
        self._writer.add_summary(self._session.run(
            self._log, feed_dict={self._log_placeholder: log}), global_step=step)

    def __enter__(self):
        self._writer = tf.summary.FileWriter(self._logdir, self._session.graph)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._writer.close()
        self._writer = None

