from ailib.utilities import spaces
from ailib.utilities.environments import wrappers

__all__ = ['Poly4']


class Poly4(wrappers.FreeJointActionWrapper):
    def __init__(self, steps=None):
        super().__init__(self._eval, spaces.Box([5]), spaces.Box([1], bound=(-4, 4)), steps=steps)

    @staticmethod
    def _eval(x):
        x = x[0]
        obs = [x ** 4, x ** 3, x ** 2, x, 1]
        return obs, -x ** 4 + x ** 3 + 2 * x ** 2 - 2 * x + 1, (None, None)
