import numpy as np

from ailib.utilities import spaces
from ailib.utilities.environments import wrappers

__all__ = ['Tang']


class Tang(wrappers.FreeJointActionWrapper):
    def __init__(self, steps=None):
        super().__init__(self._eval, spaces.Box([6]), spaces.Box([1], bound=(-2, 2)), steps=steps)

    @staticmethod
    def _eval(x):
        x = x[0]
        obs = [x, np.sin(5 * x), np.sin(10 * x), np.sin(20 * x), np.sin(40 * x), np.cos(x)]
        return obs, np.sin(5 * x) + np.sin(10 * x) + np.sin(20 * x) + np.sin(40 * x) + np.cos(x), (None, None)
