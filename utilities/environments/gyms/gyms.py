import gym

from ailib.utilities import environments, spaces

__all__ = ['CartPoleV0', 'PendulumV0']


class CartPoleV0(environments.Environment):
    @property
    def observation_space(self):
        return spaces.Box([4])

    @property
    def action_shape(self):
        return spaces.Discrete((0, 1))

    def reset(self):
        result = self._env.reset()
        if self._render:
            self._env.render()
        return result

    def step(self, action):
        result = self._env.step(action)
        if self._render:
            self._env.render()
        return result

    def __init__(self, render=False, name=None):
        super().__init__(name)
        self._render = render
        self._env = gym.make('CartPole-v0')


class PendulumV0(environments.Environment):
    def __init__(self, render=False, name=None):
        super().__init__(name)
        self._render = render
        self._env = gym.make('Pendulum-v0')

    def reset(self):
        result = self._env.reset()
        if self._render:
            self._env.render()
        return result

    def step(self, action):
        result = self._env.step(action)
        if self._render:
            self._env.render()
        return result

    @property
    def observation_space(self):
        return spaces.Space.from_gym(self._env.observation_space)

    @property
    def action_space(self):
        return spaces.Space.from_gym(self._env.action_space)
