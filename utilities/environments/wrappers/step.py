import numpy

from ailib.utilities import environments, helpers, spaces

__all__ = ['StepByStepWrapper']


class StepByStepWrapper(environments.Environment):
    @property
    def observation_space(self):
        return self._obs_space

    @property
    def action_space(self) -> spaces.Box:
        return self._act_space

    @property
    def total_steps(self):
        return self._total_steps

    def __init__(self, f, output_space: spaces.Space, input_space: spaces.Box, init_input, order=None, name=None):
        """
        Step-by-Step Wrapper will automatically wrap an function to be optimized as a environment where each action
        stands for a single value in input_space.
        :param f: The function to be optimized, the output should be outputs, credit and info, where outputs and credit
        is identical to the observation and the reward in a typical reinforcement learning process and info specfies
        some auxiliary information for showing the detailed results.
        :param output_space: The output space of the function.
        :param input_space: The input space/parameters of input space, which will be used to generate action space.
        :param init_input: The initial parameters.
        :param order: The order of the indexes given by sequential actions.
        :param name: The name of the environment.
        """
        super().__init__(name)
        self._f = f

        self._obs_space = spaces.Space.combine(output_space, spaces.Box(input_space.shape), input_space)
        self._act_space = spaces.Box(spaces.SCALAR_SHAPE, (-1, 1))

        self._raw_act_space = spaces.Box(input_space.shape, bound=(
            input_space.min_bound - init_input,
            input_space.max_bound - init_input
        ))

        self._total_steps = self._raw_act_space.channels
        self._init_action = init_input

        self._running_step = None
        self._running_action = None
        self._running_reward = None
        self._running_info = None
        self._running_init_action = None
        self._order = order if order else [i for i in range(self._raw_act_space.channels)]
        if not callable(self._init_action):
            self._init_action = numpy.asarray(self._init_action)
            self._init_obs, self._init_reward, self._init_info = self._f(self._init_action.copy())
            assert self._init_action.shape == self._raw_act_space.shape

    def reset(self):
        if callable(self._init_action):
            init_action = self._init_action()
            assert init_action.shape == self._act_space.shape

            self._running_init_action = numpy.asarray(init_action)
            init_obs, self._running_reward, self._running_info = self._f(self._running_init_action.copy())
            init_reward = self._running_reward
        else:
            self._running_init_action = self._init_action.copy()
            self._running_reward, self._running_info = self._init_reward, self._init_info
            init_obs = self._init_obs
            init_reward = self._init_reward
        self._running_step = 0
        self._running_action = helpers.normalize_with_bound(
            numpy.zeros(self._raw_act_space.shape), self._raw_act_space.bound)

        return (init_obs,
                numpy.identity(self._raw_act_space.channels)[self._order[self._running_step]],
                self._running_action), init_reward

    def step(self, action):
        assert self._running_step is not None and self._running_step < self.total_steps, \
            (self._running_step, self.total_steps)
        action = numpy.asarray(action)
        assert action.shape == self._act_space.shape
        self._running_action[self._order[self._running_step]] = action
        actual_action = self._running_init_action + helpers.denormalize_with_bound(
            self._running_action, self._raw_act_space.bound)
        obs, actual_reward, info = self._f(actual_action.copy())
        incremental_reward = actual_reward - self._running_reward
        self._running_reward = actual_reward
        index = numpy.identity(self._raw_act_space.channels)[self._order[self._running_step]]
        self._running_step += 1
        return (obs, index, self._running_action), incremental_reward, self._running_step >= self._total_steps, info

    @property
    def cumulative_action(self):
        return self._running_init_action + helpers.denormalize_with_bound(
            self._running_action, self._raw_act_space.bound)
