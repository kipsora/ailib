import numpy

from ailib.utilities import environments, spaces

__all__ = ['BoundedJointActionWrapper']


class BoundedJointActionWrapper(environments.Environment):
    @property
    def observation_space(self):
        return self._obs_space

    @property
    def action_space(self) -> spaces.Box:
        return self._act_space

    @property
    def total_steps(self):
        return self._total_steps

    def __init__(self, f, output_space: spaces.Space, input_space: spaces.Box, init_input, steps=None, name=None):
        """
        Joint Action Wrapper will automatically wrap a function as a environment where each action being jointly given
        by agent stands for a delta accumulated from an initial given action.
        :param f: The function to be optimized, the output should be outputs, credit and info, where outputs and credit
        is identical to the observation and the reward in a typical reinforcement learning process and info specfies
        some auxiliary information for showing the detailed results.
        :param output_space: The output space of the function.
        :param input_space: The input space/parameters of input space, which will be used to generate action space.
        :param init_input: The initial parameters.
        :param steps: The number of steps the environment to be performed.
        :param name: The name of the environment.
        """
        super().__init__(name)
        self._f = f

        self._obs_space = output_space
        self._total_steps = steps if steps else 10
        self._act_space = spaces.Box(input_space.shape, bound=(
            (input_space.min_bound - init_input) / self._total_steps,
            (input_space.max_bound - init_input) / self._total_steps
        ))
        self._init_action = init_input

        self._running_step = None
        if callable(self._init_action):
            self._running_action = None
            self._running_reward = None
            self._running_info = None
        else:
            self._init_action = numpy.asarray(self._init_action, dtype='float64')
            self._init_obs, self._init_reward, self._init_info = self._f(self._init_action.copy())
            assert self._init_action.shape == self._act_space.shape

    def reset(self):
        if callable(self._init_action):
            init_action = self._init_action()
            assert init_action.shape == self._act_space.shape

            self._running_action = numpy.asarray(init_action, dtype='float64')
            init_obs, self._running_reward, self._running_info = self._f(self._running_action.copy())
            init_reward = self._running_reward
        else:
            self._running_action = self._init_action.copy()
            self._running_reward, self._running_info = self._init_reward, self._init_info
            init_obs = self._init_obs
            init_reward = self._init_reward
        self._running_step = 0
        return init_obs, init_reward

    def step(self, action):
        assert self._running_step is not None and self._running_step < self.total_steps
        self._running_step += 1
        action = numpy.asarray(action)
        assert action.shape == self._act_space.shape
        self._running_action += action
        obs, actual_reward, info = self._f(self._running_action.copy())
        incremental_reward = actual_reward - self._running_reward
        self._running_reward = actual_reward
        return obs, incremental_reward, self._running_step >= self._total_steps, info

    @property
    def cumulative_action(self):
        return self._running_action.copy()
