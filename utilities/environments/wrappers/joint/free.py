import numpy as np

from ailib.utilities import environments, spaces

__all__ = ['FreeJointActionWrapper']


class FreeJointActionWrapper(environments.Environment):
    @property
    def observation_space(self):
        return self._obs_space

    @property
    def action_space(self) -> spaces.Box:
        return self._act_space

    @property
    def total_steps(self):
        return self._total_steps

    def __init__(self, f, output_space: spaces.Space, input_space: spaces.Box, steps=None, name=None):
        """
        Free Joint Action Wrapper will automatically wrap a function as a environment where each action being jointly
        given by agent stands for a delta accumulated from an initial given action.
        :param f: The function to be optimized, the output should be outputs, credit and info, where outputs and credit
        is identical to the observation and the reward in a typical reinforcement learning process and info specfies
        some auxiliary information for showing the detailed results.
        :param output_space: The output space of the function.
        :param input_space: The input space/parameters of input space, which will be used to generate action space.
        :param steps: The number of steps the environment to be performed.
        :param name: The name of the environment.
        """
        super().__init__(name)
        self._f = f

        self._output_space = output_space
        self._obs_space = spaces.Space.combine(output_space)
        self._total_steps = steps if steps else 10
        self._act_space = spaces.Box(input_space.shape, (
            0, (input_space.max_bound - input_space.min_bound) / self._total_steps
        ))
        self._input_space = input_space

        self._running_step = None
        self._running_action = None
        self._running_reward = None
        self._running_info = None

    def reset(self):
        init_action = self._act_space.sample()
        assert init_action.shape == self._act_space.shape

        self._running_action = np.asarray(init_action, dtype='float64')
        init_obs, actual_reward, self._running_info = self._f(self._running_action + self._input_space.min_bound)
        self._running_step = 0
        self._running_reward = 0
        if isinstance(self._output_space, spaces.BasicSpace):
            return init_obs, actual_reward
        else:
            return tuple(init_obs), actual_reward

    def step(self, action):
        assert self._running_step is not None and self._running_step < self.total_steps
        self._running_step += 1
        action = np.asarray(action)
        assert action.shape == self._act_space.shape
        self._running_action += action
        obs, actual_reward, info = self._f(self._running_action + self._input_space.min_bound)
        incremental_reward = actual_reward - self._running_reward
        self._running_reward = actual_reward
        if isinstance(self._output_space, spaces.BasicSpace):
            return obs, incremental_reward, self._running_step >= self._total_steps, info
        else:
            return tuple(obs), incremental_reward, self._running_step >= self._total_steps, info

    @property
    def cumulative_action(self):
        return self._running_action + self._input_space.min_bound
