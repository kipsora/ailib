import abc

from ailib.utilities import spaces

__all__ = ['Environment']


class Environment(object, metaclass=abc.ABCMeta):
    def __init__(self, name=None):
        self._name = name if name else self.__class__.__name__

    @property
    def name(self):
        return self._name

    @abc.abstractmethod
    def reset(self):
        pass

    @abc.abstractmethod
    def step(self, action):
        pass

    @property
    def observation_space(self) -> spaces.Space:
        raise SyntaxError("This environment has not implemented observation_space.")

    @property
    def action_space(self) -> spaces.Space:
        raise SyntaxError("This environment has not implemented action_space.")

    @property
    def rnn_loops(self):
        raise SyntaxError("This environment has not implemented rnn_loop.")

    def rnn_preprocessor(self, obs):
        """
        Translate an observation to a ordered sequence of observation
        :param obs: The observation vector
        :return: A stacked observation vector
        """
        raise SyntaxError("This environment has not implemented get_rnn_preprocessor.")

    def rnn_postprocessor(self, hidden):
        """
        The RNN will output `loops' vectors with equally size (defined in the actor). But one may want
        to translate them into a group of actions.
        :param hidden: The vectors (of length loops) output by RNN
        :return: The processed result
        """
        raise SyntaxError("This environment has not implemented get_rnn_postprocessor.")
