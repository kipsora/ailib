import datetime
import logging
import sys
import time
import os
import collections
import numpy
import random
import tensorflow as tf

__all__ = [
    'get_activation_from_str',
    'convert_to_shape',
    'broadcast',
    'convert_to_bound',
    'get_tf_session',
    'get_tf_abs_scope_name',
    'get_logger',
    'normalize_with_bound',
    'denormalize_with_bound',
    'get_datetime',
    'get_relative_path',
    'set_seed',
    'START_DATETIME',
    'set_tf_verbosity'
]


def get_activation_from_str(method):
    if method == 'sigmoid':
        return tf.nn.sigmoid
    elif method == 'relu':
        return tf.nn.relu
    elif method == 'tanh':
        return tf.nn.tanh
    else:
        raise NotImplementedError


def convert_to_shape(shape):
    if isinstance(shape, numpy.ndarray):
        return tuple(shape.tolist())
    elif isinstance(shape, int):
        return shape,
    elif isinstance(shape, collections.Iterable):
        return tuple(shape)
    else:
        raise NotImplementedError


def broadcast(value, shape):
    if isinstance(value, int) or isinstance(value, float):
        value = numpy.ones(shape) * value
    else:
        value = numpy.asarray(value)
        assert value.shape == shape, (value.shape, shape)
    return value


def convert_to_bound(bound, shape, keep_numpy=False):
    if isinstance(bound, int) or isinstance(bound, float):
        bound = [-bound, bound]
    min_bound = broadcast(bound[0], shape)
    max_bound = broadcast(bound[1], shape)
    if keep_numpy:
        return min_bound, max_bound
    elif shape == ():
        return min_bound, max_bound
    else:
        return tuple(min_bound.tolist()), tuple(max_bound.tolist())


def get_tf_session(log_device_placement=False, allow_soft_placement=True, allow_gpu_growth=True):
    config = tf.ConfigProto(log_device_placement=log_device_placement, allow_soft_placement=allow_soft_placement)
    config.gpu_options.allow_growth = allow_gpu_growth
    return tf.Session(config=config)


def get_tf_abs_scope_name(name):
    return tf.get_variable_scope().name + '/' + name if tf.get_variable_scope().name else name


def get_logger(name, log_path=None, log_stream=sys.stderr, log_level=logging.INFO):
    logger = logging.getLogger(name)
    formatter = logging.Formatter()
    if log_stream:
        if log_stream in ['stdout', 'out', 'o']:
            log_stream = sys.stdout
        elif log_stream in ['stderr', 'err', 'e']:
            log_stream = sys.stderr
        else:
            assert NotImplementedError
        handler = logging.StreamHandler(stream=log_stream)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    if log_path:
        log_path = os.path.abspath(log_path)
        dirname = os.path.dirname(log_path)
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        handler = logging.FileHandler(log_path)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    logger.setLevel(log_level)
    return logger


def normalize_with_bound(value, bound, name=None):
    min_bound, max_bound = numpy.asarray(bound[0]), numpy.asarray(bound[1])
    return tf.divide(2 * value - (min_bound + max_bound), max_bound - min_bound, name=name)


def denormalize_with_bound(value, bound, name=None):
    min_bound, max_bound = numpy.asarray(bound[0]), numpy.asarray(bound[1])
    return tf.divide(value * (max_bound - min_bound) + (min_bound + max_bound), 2.0, name=name)


def get_datetime():
    return datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S %f')


def get_relative_path(path):
    return os.path.relpath(os.path.abspath(path), os.path.abspath('./'))


START_DATETIME = get_datetime()


def set_seed(seed):
    if seed is None:
        seed = numpy.random.randint(0, (1 << 32) - 1)
    tf.set_random_seed(seed)
    numpy.random.seed(seed)
    random.seed(seed)
    return seed


def set_tf_verbosity(verbosity='3'):
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = verbosity
