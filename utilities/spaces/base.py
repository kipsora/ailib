import abc
from abc import ABCMeta

import numpy as np

from ailib.utilities import helpers

__all__ = ['SCALAR_SHAPE', 'Space', 'BasicSpace', 'HybridSpace']

SCALAR_SHAPE = ()


class Space(object, metaclass=abc.ABCMeta):
    def __init__(self):
        pass

    @staticmethod
    def combine(*spaces: 'Space'):
        tmp_spaces = []
        for space in spaces:
            if isinstance(space, HybridSpace):
                tmp_spaces.extend(space.spaces)
            else:
                tmp_spaces.append(space)
        assert len(tmp_spaces) > 0
        if len(tmp_spaces) == 1:
            return tmp_spaces[0]
        return HybridSpace(*tmp_spaces)

    @property
    @abc.abstractmethod
    def zeros(self):
        pass

    @abc.abstractmethod
    def sample(self):
        pass

    @staticmethod
    def from_gym(space):
        from gym import spaces as gym_space
        import collections
        if isinstance(space, gym_space.Box):
            from ailib.utilities.spaces import Box
            return Box(space.shape, (space.low, space.high), space.dtype)
        elif isinstance(space, gym_space.Discrete):
            from ailib.utilities.spaces import Discrete
            return Discrete(list(range(space.n)), space.dtype)
        elif isinstance(space, collections.Iterable):
            return HybridSpace(*[Space.from_gym(sub_space) for sub_space in space])
        else:
            raise NotImplementedError


class BasicSpace(Space, metaclass=ABCMeta):
    def __init__(self, shape, dtype):
        super().__init__()
        self._shape = helpers.convert_to_shape(shape)
        self._dtype = dtype

    @property
    def shape(self):
        return self._shape

    @property
    def dtype(self):
        return self._dtype

    @property
    def channels(self):
        return int(np.prod(self._shape))

    @property
    def zeros(self):
        return np.zeros(self._shape, self._dtype)


class HybridSpace(Space):
    def __init__(self, *spaces: BasicSpace):
        super().__init__()
        self._spaces = spaces

    @property
    def shapes(self):
        for space in self._spaces:
            yield space.shape

    @property
    def spaces(self):
        for space in self._spaces:
            yield space

    @property
    def dtypes(self):
        for space in self._spaces:
            yield space.dtype

    @property
    def zeros(self):
        return tuple(space.zeros for space in self._spaces)

    def sample(self):
        return tuple(space.sample() for space in self._spaces)
