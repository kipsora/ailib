import numpy as np

from ailib.utilities.spaces import BasicSpace, SCALAR_SHAPE

__all__ = ['Discrete']


class Discrete(BasicSpace):
    def __init__(self, options, dtype=None):
        super().__init__(SCALAR_SHAPE, dtype if dtype else 'int32')
        self._options = options

    def sample(self):
        return self._options[np.random.choice(len(self._options))]
