import numpy as np

from ailib.utilities.helpers import helpers
from ailib.utilities.spaces import BasicSpace

__all__ = ['Box']


class Box(BasicSpace):
    def __init__(self, shape, bound=None, dtype=None):
        super().__init__(shape, dtype if dtype else 'float32')

        self._bound = helpers.convert_to_bound(bound if bound else np.inf, self._shape, keep_numpy=True)

    @property
    def min_bound(self):
        return self._bound[0].copy()

    @property
    def max_bound(self):
        return self._bound[1].copy()

    @property
    def bound(self):
        return self.min_bound, self.max_bound

    def sample(self):
        return np.random.rand(*self.shape) * (self._bound[1] - self._bound[0]) + self._bound[0]
